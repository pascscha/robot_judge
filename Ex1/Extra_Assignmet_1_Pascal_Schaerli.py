# /usr/bin/python3

import pandas as pd
import numpy as np


def get_trans_matrix(D, columns):
    """Gets the transformation Matrix to get D into the dictionary space
    @param D:       A pandas Dataframe holding the frequency of words
    @param columns: The names of the columns we want to transform D to
    @returns        Transition Matrix so that D * T has all the columns at the
                    place defined by columns
    """
    T = np.zeros((D.shape[1], len(columns)))
    for i in range(len(columns)):
        # Check if D has the current column, if yes make an entry in T
        if columns[i] in D.columns.values:
                # This kind of ugly line gets the correct row index
            j = list(D.columns.values).index(columns[i])
            T[j][i] = 1
    return T


def get_transformation(D1, D2):
    """Gets the transition matrices needed to make D1 and D2
       lie in the same dictionary space
    @param D1: A pandas Dataframe of the first matrix
    @param D2: A pandas Dataframe of the first matrix
    @returns   The transformation Matrices
    """
    # Get titles of both columns
    cols1 = D1.columns.values
    cols2 = D2.columns.values

    # Merge column titles (without duplicates)
    cols = list(cols1) + list(set(cols2) - set(cols1))
    # Calculate Transition matrices
    T1 = get_trans_matrix(D1, cols)
    T2 = get_trans_matrix(D2, cols)
    return T1, T2


def cos_sim(v1, v2):
    """Calculates the cosine similarity of two vectors
    @param v1: numpy array of the first vector
    @param v2: numpy array of the second vector
    @returns   The cosine similarity of v1 and v2
    """
    numerator = v1.dot(v2)
    denumerator = np.linalg.norm(v1) * np.linalg.norm(v2)
    return numerator / denumerator


def cosine_sim(D1, D2):
    """Gets two Matrices in the same dictionary space and calculates
       cosine similarity of them
    @param D1: pandas dataframe of the first matrix
    @param D2: pandas dataframe of the second matrix
    @returns   confusion matrix with the cosine similarities of all
               rows in D1 and D2
    """
    # Get transformation matrices
    T1, T2 = get_transformation(D1, D2)

    column_names = D1.index.values
    row_names = D2.index.values

    # Apply transformation and convert to numpy matrices
    D1 = D1.dot(T1).values
    D2 = D2.dot(T2).values

    # Initialize confusion matrix as indentity matrix
    cosine_similarities = np.ndarray((D1.shape[0], D2.shape[0]))

    # Iterate over half of the matrix
    for i in range(D1.shape[0]):
        for j in range(D2.shape[0]):
            # Compute entry
            cs = cos_sim(D1[i], D2[j])
            cosine_similarities[i][j] = cs

    # Return pandas dataframe with calculated similarities
    return pd.DataFrame(cosine_similarities, columns=row_names, index=column_names)


def main():
    """Here We create two example dataframes to see our function in action!
    """
    # Example Document Features
    data1 = [[1, 1, 0],
             [0, 1, 0]]
    data2 = [[0, 1],
             [3, 2],
             [5, 3]]

    # Convert to dataframes with different column names.
    D1 = pd.DataFrame(data1,
                      columns=["feature 1", "feature 3", "feature 4"],
                      index=["doc1", "doc2"])
    D2 = pd.DataFrame(data2,
                      columns=["feature 2", "feature 3"],
                      index=["doc3", "doc4", "doc5"])

    # Calculate confusion Matrix
    cos_sim = cosine_sim(D1, D2)

    # Output
    print("INPUTS:\n{}\n\n{}\n\nCOSINE SIMILARITIES:\n{}".format(D1, D2, cos_sim))


if __name__ == "__main__":
    main()

"""
Expected Output:

INPUTS:
      feature 1  feature 3  feature 4
doc1          1          1          0
doc2          0          1          0

      feature 2  feature 3
doc3          0          1
doc4          3          2
doc5          5          3

COSINE SIMILARITIES:
          doc3      doc4      doc5
doc1  0.707107  0.392232  0.363803
doc2  1.000000  0.554700  0.514496
"""
